function fetchByParam() {
    const params = new Proxy(new URLSearchParams(window.location.search), {
        get: (searchParams, prop) => searchParams.get(prop),
    });

    let value = params.page

    if (value) {
        fetchContent('/' + value);
    }
}

function fetchByHash() {
    if (window.location.hash) {
        fetchContent('/' + window.location.hash.replaceAll('#', ''));
    } else {
        fetchByParam();
    }
}

function fetchContent(uri) {
    progress.open();
    fetch(uri)
        .then(res => {
            return res.text();
        })
        .then(text => {
            document.querySelector('#content').innerHTML = text;
            progress.close();
        })
}