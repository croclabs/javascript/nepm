let drawer = mdc.drawer.MDCDrawer.attachTo(document.querySelector(".mdc-drawer"));
document.addEventListener('click', (event) => {
    if (drawer.open) {
        closeMenu();
    }
});

document.querySelector("#menu-toggle").addEventListener('click', (event) => {
    event.stopPropagation();
    toggleMenu();
});

function openMenu() {
    drawer.foundation.open();
    document.querySelector("#menu-toggle").innerHTML = "close";
}

function closeMenu() {
    drawer.foundation.close();
    document.querySelector("#menu-toggle").innerHTML = "menu";
}

function toggleMenu() {
    if (drawer.open) {
        closeMenu();
    } else {
        openMenu();
    }
}